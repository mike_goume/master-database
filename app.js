const express = require('express');
const app = express();
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');

//MiddleWares
app.use(cors());
app.use(bodyParser.json());

// Import Routes
const postsRoute = require('./routes/posts');
app.use('/posts', postsRoute);
const usersRoute = require('./routes/users');
app.use('/users', usersRoute);
const housesRoute = require('./routes/houses');
app.use('/houses', housesRoute);


// ROUTES
app.get('/', (req, res) => {
    res.send('We are on home');
});

// connect to db here
mongoose.connect(
    process.env.DB_CONNECTION,
    { useNewUrlParser: true },
     () => console.log('connected to DB')
);






 



app.listen(3000);