const express = require('express');
const router = express.Router();
const Post = require('../models/Post');

router.get('/', async (req, res) => {
    try {
        const posts = await Post.find();
        res.json(posts);
    } catch (error) {
        res.json({message:error})
    }
});

router.post('/', async (req, res) => {
    console.log(req.body);
    const post = new Post({
        title: req.body.title,
        description: req.body.description
    });
    try {
        const savedPost = await post.save();
        res.json(savedPost);    
    } catch (error) {
        res.json({ message: error });
    }
});

router.get('/:postID', async (req, res) => {
    try {
        const post = await Post.findById(req.params.postID);
        res.json(post);
    } catch (error) {
        res.json({message:error})
    }
});


router.delete('/:postID', async (req, res) => {
    try {
        const rem_post = await Post.remove({_id: req.params.postID});
        res.json(rem_post);
    } catch (error) {
        res.json({message:error})
    }
});

router.patch('/:postID', async (req, res) => {
    try {
        const up_post = await Post.updateOne(
            { _id: req.params.postID },
            { $set: { title: req.body.title } }
        );
        res.json(up_post);
    } catch (error) {
        res.json({message:error})
    }
});


module.exports = router;