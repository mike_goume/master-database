const express = require('express');
const router = express.Router();
const House = require('../models/House');


//GET ALL HOUSES
router.get('/', async (req, res) => {
    try {
        const houses = await House.find();
        res.json(houses);
    } catch (error) {
        res.json({message:error})
    }
});
//GET A HOUSE BASED ON HOUSE ID
router.get('/:id', async (req, res) => {
    House.findOne({_id: req.params.id }, function(err, result) {
        if (err) {
            res.send(err);
        } else {
            res.send(result);
        }
    });
})
//GET HOUSES BASED ON PLACE
router.get('/getHouseByLocation/:location', async (req, res) => {
    console.log('yey')
})
//CREATE A NEW HOUSE
router.post('/', async (req, res) => {
    const newhouse = new House({
        title: req.body.title,
        address: req.body.address,
        region: req.body.region,
        city: req.body.region,
        street: req.body.street,
        type: req.body.type,
        guests: req.body.guests,
        rooms: req.body.rooms,
        bedNo: req.body.bedNo,
        lon: req.body.lon,
        lat: req.body.lat,
        price: req.body.price,
        images: req.body.images
    });
    try {
        const savedHouse = await newhouse.save();
        res.json(savedHouse);    
    } catch (error) {
        res.json({ message: error });
    }
});
//DELETE A HOUSE BASED ON HOUSE ID
router.delete('/:id', async (req, res) => {
    try {
        const rem_house = await House.remove({_id: req.params.id});
        res.json(rem_house);
    } catch (error) {
        res.json({message:error})
    }
});
//UPDATE THE VALUES OF A HOUSE BASED ON HOUSE ID
router.patch('/:id', async (req, res) => {
    try {
        const up_house = await House.updateOne(
            { _id: req.params.id },
            { $set: { price: req.body.price } }
        );
        res.json(up_house);
    } catch (error) {
        res.json({message:error})
    }
});

module.exports = router;