const express = require('express');
const router = express.Router();
const User = require('../models/User');



//CREATE A NEW USER WITH EMAIL
router.post("/signUp", async (req, res) => {
    const newuser = {
        fname: req.body.fname,
        lname: req.body.lname,
        username: req.body.username,
        email: req.body.email,
        password: req.body.password,
        image: 'undefined',
    }
    User.create(newuser, function(err, result) {
        if (err) {
            console.log(err);
            res.send("Unique");
        } else {
            res.send(newuser);
        }
    });
});
//CREATE A NEW USER WITH SOCIAL MEDIA
router.post("/signUpSM", async (req, res) => {
    const newuser = {
        username: req.body.username,
        email: req.body.email,
    }
    User.create(newuser, function(err, result) {
        if (err) {
            console.log(err);
            res.send("Unique");
        } else {
            res.send(newuser);
        }
    });
});
router.post('/loginUser', async (req, res) => {
    console.log('Body of requested user to login: ', req.body);
    const newuser = {
        email: req.body.email,
        password: req.body.password,
    }
    User.create(newuser, async function(err, result) {
        if (err) {
            const foundAccount = await User.find ({ "email" : req.body.email ,"password": req.body.password});
            console.log('The account found in database: ', foundAccount);
            if(foundAccount.length !== 0) res.send(foundAccount[0]);
            else res.send("Invalid Email or Password, Try again");
        } else {
            const foundAccount = await User.findOne ({ "email" : req.body.email ,"password": req.body.password});
            if(foundAccount) res.send(foundAccount);
        }
    })
});
//GET ALL USERS
router.get('/getUsers', async (req, res) => {
    try {
        const users = await User.find();
        res.json(users);
    } catch (error) {
        res.json({ message:error });
    }
});
//GET A USER WITH A SPECIFIC ID
router.get('/getUsers/:userID', async (req, res) => {
    try {
        const user = await User.findById(req.params.userID);
        res.json(user);
    } catch (error) {
        res.json({message:error});
    }
})
//DELETE A USER WITH A SPECIFIC ID
router.delete('/deleteUser/:userID', async (req, res) => {
    try {
        const rem_user = await User.remove({_id: req.params.userID});
        res.json(rem_user);
    } catch (error) {
        res.json({message:error})
    }
});

module.exports = router;