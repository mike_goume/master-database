const mongoose = require('mongoose');

const UserSchema = mongoose.Schema(
  {
    fname: {
      type: String
    },
    lname : {
      type: String
    },
    username : {
      type: String
    },
    email:{
      type: String,
      unique: true 
    },
    password:{
      type: String
    },
    image:{
      type: String
    }
  },
);

module.exports = mongoose.model('Users', UserSchema);