const mongoose = require('mongoose');

const HouseSchema = mongoose.Schema({
    user_id: {
        type: String,
        required: false
    },
    title: {
        type: String,
        required: true
    },
    address: {
        type: String,
        required: true
    },
    region: {
        type: String,
        required: true
    },
    city: {
        type: String,
        required: true
    },
    street: {
        type: String,
        required: true
    },
    type: {
        type: String,
        required: true
    },
    guests: {
        type: Number,
        required: true
    },
    rooms: {
        type: Number,
        required: true
    },
    bedNo: {
        type: Number,
        required: true
    },
    lon: {
        type: String,
        required: false
    },
    lat: {
        type: String,
        required: false
    },
    price: {
        type: String,
        required: true
    },
    rating: {
        type: String,
        required: false,
        default: 0,
    },
    reviews: {
        type: Array,
        required: false,
        default: [],
    },
    images: {
        type: Array,
        required: false,
        default: [],
    }
});

module.exports = mongoose.model('Houses', HouseSchema);